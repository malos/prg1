//
//  CommonData.cpp
//  bbk
//
//  Created by weekfield2 on 2014/08/30.
//
//

#include "CommonData.h"

USING_NS_CC;

typedef std::pair<std::string,int> cis;
typedef std::pair<std::string,Value > is;



/*
-(void) keyGetPutSub:(bool)getFg dic:(NSMutableDictionary*) persistentDic ;
//-(void) intKeySub:(bool) getFg dic:(NSMutableDictionary*)persistentDic idNo:(id)idNo akey:(id <NSCopying>)akey ;
-(void) intKeySub:(bool) getFg dic:(NSMutableDictionary*) persistentDic idNo:(int*)idNo akey:(NSString*)akey;
*/

static CommonData* _sharedCommonData;



// 複数のクラスから同一のインスタンスを扱うことをできるようにするためシングルトンにする
CommonData* CommonData::sharedCommonData() {

		if (_sharedCommonData == NULL) {
			_sharedCommonData = new CommonData();
		}
	return _sharedCommonData;
};

void CommonData::save(){
    
    //  Documentsディレクトリのパスを受け取る。
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
    
//    auto fileUtils = CCFileUtils::sharedFileUtils()->getWritablePath();
//    std::string  filePath=  CCFileUtils::getInstance()->getWritablePath();
    
    
   //  gameData = FileUtils::getInstance()->getValueMapFromFile("data.plist");
    

    //  コンソールにDocumentsディレクトリのパスを表示
//    printf("documentsDirectory = %s\n", filePath.c_str());
    
//    auto str = Value("string");
//    cocos2d::Map<std::string, int> dectionary;
    
    
    
//    for (auto it = dectionary.begin(): it != dectionary.end(); ++it) (*it).;
    //cocos2d::Map
    
    
//    dectionary.insert("Hiscore", 100);
    
//    int highScore = 100;
//    CCUserDefault::getInstance()->setIntegerForKey("highScore", highScore);
//    CCUserDefault::getInstance()->flush();
    
//    int highScore = CCUserDefault::sharedUserDefault()->getIntegerForKey("highScore", 0);
    
    
    std::string  filePath=  CCFileUtils::getInstance()->getWritablePath();
    filePath.append("GameSaveData.plist");
    //  コンソールにDocumentsディレクトリのパスを表示
    //    printf("documentsDirectory = %s\n", filePath.c_str());
    
    //load
    ValueMap values = FileUtils::getInstance()->getValueMapFromFile(filePath.c_str());

    keyGetPutSub(false,&values);
    
    //list_mapの中身のkeyNameと言う名のKeyを指定してstring型を取得する
    
/*
    //  Documents/persistentDic.plistパス文字列作成。
    NSString* filePath = [documentsDirectory stringByAppendingPathComponent:@"GameSaveData.plist"];
    
    //  ファイルからNSMutableDictionary作成。
    NSMutableDictionary* persistentDic = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];
    if (!persistentDic) {//出来なかったら初期化
        persistentDic = [[[NSMutableDictionary alloc]init] autorelease];
    }

 
 
 //  整数値を収納させる。
    //ここに追加して行く。
    //    [persistentDic setObject:[NSNumber numberWithInt:_hiscore] forKey:@"Hiscore"];
    [self keyGetPutSub:NO dic:persistentDic ];
    
    
    printf("hiscore = %d\n", hiscore);
    //  ファイルに保存。
    [persistentDic writeToFile:filePath atomically:YES];
*/
}

void CommonData::load(){

    
    std::string  filePath=  CCFileUtils::getInstance()->getWritablePath();
    filePath.append("GameSaveData.plist");
    //  コンソールにDocumentsディレクトリのパスを表示
//    printf("documentsDirectory = %s\n", filePath.c_str());
    
    //load
    ValueMap values = FileUtils::getInstance()->getValueMapFromFile(filePath.c_str());
    
    //list_mapの中身のkeyNameと言う名のKeyを指定してstring型を取得する
    log("key指定:%s", values.at("keyName").asString().c_str());

    
    keyGetPutSub(true,&values);

    
    
/*
    
//-(void) load{
    
    //  Documentsディレクトリのパスを受け取る。
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    //  コンソールにDocumentsディレクトリのパスを表示
    printf("documentsDirectory = %s\n", [documentsDirectory UTF8String]);
    //  Documents/persistentDic.plistパス文字列作成。
    NSString* filePath = [documentsDirectory stringByAppendingPathComponent:@"GameSaveData.plist"];
    
    //  ファイルからNSMutableDictionary作成。
    NSMutableDictionary* persistentDic = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];
    if (!persistentDic) {//なかったら作る
        persistentDic = [[[NSMutableDictionary alloc]init] autorelease];
    }
    //  整数値取り出し。
    //    _hiscore = [[persistentDic objectForKey:@"Hiscore"] intValue];
    
    [self keyGetPutSub:YES dic:persistentDic ];
    
    
    printf("hiscore = %d\n", hiscore);
 
 */
}


//保存すべき情報をここで一括して保存
//最小限の実装で済むようにした

// -(void) keyGetPutSub:(bool)getFg dic:(NSMutableDictionary*) persistentDic {
void CommonData::keyGetPutSub(bool getFg , cocos2d::ValueMap* persistentDic) {
  
    
    
    /////////////////////////////////////////////////////////////////
    //環境
    /////////////////////////////////////////////////////////////////
    //ハイスコア
//    [self intKeySub:getFg dic:persistentDic idNo:&hiscore  akey:@"Hiscore"];
    
//    std::string  a =  std::string("Hiscore");
    
    intKeySub(getFg,persistentDic,&hiscore,"Hiscore");

    
    //今のエリアＮＯ（この情報で敵や初期状態を決める）
	intKeySub(getFg ,persistentDic,&nowAreaNo ,"NowAreaNo");
    
    //今のステージＮＯ
	intKeySub(getFg ,persistentDic,&nowStageNo ,"NowStageNo");
    
    //クリアしたステージＮＯ マップでのステージの表示に必要　複数あるのでビット操作か？
	intKeySub(getFg ,persistentDic,&clearStageBitNo ,"ClearStageBitNo");
    
    
    
    /////////////////////////////////////////////////////////////////
    //敵の持ち物
    /////////////////////////////////////////////////////////////////
    
    //ボーナス１ 敵がドロップするボーナス種類１
	intKeySub(getFg ,persistentDic,&enemyBonusKind1 ,"EnemyBonusKind1");
    //ボーナス２ 敵がドロップするボーナス種類２
	intKeySub(getFg ,persistentDic,&enemyBonusKind2 ,"EnemyBonusKind2");
    //ボーナス３ 敵がドロップするボーナス種類３
	intKeySub(getFg ,persistentDic,&enemyBonusKind3 ,"EnemyBonusKind3");
    
    //ボーナス１ 敵がドロップするボーナス数１
	intKeySub(getFg ,persistentDic,&enemyBonusNo1 ,"EnemyBonusNo1");
    //ボーナス２ 敵がドロップするボーナス数２
	intKeySub(getFg ,persistentDic,&enemyBonusNo2 ,"EnemyBonusNo2");
    //ボーナス３ 敵がドロップするボーナス数３
	intKeySub(getFg ,persistentDic,&enemyBonusNo3 ,"EnemyBonusNo3");
    
    //倒した敵の種類
	intKeySub(getFg ,persistentDic,&enemyNameNo ,"EnemyNameNo");
    
    
    
    /////////////////////////////////////////////////////////////////
    //自分の持ち物
    /////////////////////////////////////////////////////////////////
    
    //火のエレメント
	intKeySub(getFg ,persistentDic,&myFireElement ,"MyFireElement");
    //水のエレメント
	intKeySub(getFg ,persistentDic,&myWaterElement ,"MyWaterElement");
    //木のエレメント
	intKeySub(getFg ,persistentDic,&myWoodElement ,"MyWoodElement");
    //コイン
	intKeySub(getFg ,persistentDic,&myCoin ,"MyCoin");
    
    //自分のメインキャラ
	intKeySub(getFg ,persistentDic,&myMainChara ,"MyMainChara");
    
    //持っているマイキャラ１
	intKeySub(getFg ,persistentDic,&myChara1 ,"MyChara1");
    //持っているマイキャラ２
	intKeySub(getFg ,persistentDic,&myChara2 ,"MyChara2");
    //持っているマイキャラ３
	intKeySub(getFg ,persistentDic,&myChara3 ,"MyChara3");
    
    /////////////////////////////////////////////////////////////////
    
    
    
}


//保存用サブルーチン
//-(void) intKeySub:(bool) getFg dic:(NSMutableDictionary*) persistentDic idNo:(int*)idNo akey:(NSString*)akey {
void CommonData::intKeySub(bool getFg, cocos2d::ValueMap*  persistentDic, int* idNo, std::string akey) {

    
//    if(getFg)    *idNo = [[persistentDic objectForKey:akey] intValue];
//        else [persistentDic setObject:[NSNumber numberWithInt:*idNo] forKey:akey];
    
//    persistentDic->at(akey).asString().data());

    
    if(getFg)
    *idNo = persistentDic->at(akey).asInt();
    else
        persistentDic->insert(is{akey,Value(*idNo)});
}

/*
-(void) dealloc {
	[_sharedCommonData dealloc];
	[super dealloc];
}

 
 */



