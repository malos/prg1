//
//  BaseMainScreen.h
//  bbk
//
//  Created by weekfield2 on 2014/08/07.
//
//

#ifndef __bbk__BaseMainScreen__
#define __bbk__BaseMainScreen__

//#include <iostream>


#include "cocos2d.h"
class BaseMainScreen : public cocos2d::Layer
{
public:
//#import <Foundation/Foundation.h>
//#import "cocos2d.h"

//スプライトのタグ
typedef enum
{
    TAG_BASE_MAIN_SCREEN_MY_STATUS,
    TAG_BASE_MAIN_SCREEN_FIGHTER_STATUS,
    TAG_BASE_MAIN_SCREEN_BASE_PARTS_BATCH,
    TAG_BASE_MAIN_SCREEN_BASE_PARTS_ARROW_L,
    TAG_BASE_MAIN_SCREEN_BASE_PARTS_ARROW_R,
    
    TAG_BASE_MAIN_SCREEN_ITEM1_DISP,
    TAG_BASE_MAIN_SCREEN_ITEM2_DISP,
    TAG_BASE_MAIN_SCREEN_ITEM3_DISP,
    
} baseMainScreenTags;
//ステップ
typedef enum
{
    STEP_BASE_MAIN_SCREEN_DISP_START,
    STEP_BASE_MAIN_SCREEN_DISP_WAIT,
    STEP_BASE_MAIN_SCREEN_ROTATE_L,//左にファイター画面を回す
    STEP_BASE_MAIN_SCREEN_ROTATE_L2,//左にファイター画面を回す
    STEP_BASE_MAIN_SCREEN_ROTATE_R,//左にファイター画面を回す
    STEP_BASE_MAIN_SCREEN_ROTATE_R2,//左にファイター画面を回す
    
    
    STEP_BASE_MAIN_SCREEN_OK_WAIT,
    
} baseMainScreenStep;

    


//@interface BaseMainScreen : CCLayer {
    
    baseMainScreenStep  _mainStep;
    int  _fightLayerOffsetX; // ファイトレイヤーのオフセット
    
//}
static cocos2d::Scene* createScene();
virtual bool init();
    
// a selector callback
void menuCloseCallback(cocos2d::Ref* pSender);

//void change(Ref* pSender);
void GoTitle(Ref* pSender);
    
    
    // implement the "static create()" method manually
    CREATE_FUNC(BaseMainScreen);

    
    //+(CCScene *) scene;

//@end

};




#endif /* defined(__bbk__BaseMainScreen__) */
