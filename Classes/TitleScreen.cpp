//
//  TitleScreen.m
//  blockRpg1
//
//  Created by 丸山 修 on 2013/04/29.
//  Copyright 2013年 maro. All rights reserved.
//
//メインのタイトルを管理するクラス

#include "TitleScreen.h"
#include "IntroScreen.h"
//#include "IntroScreen.h"
//#import "AreaSelectScreen.h"
//#import "PurchaseScreen.h"
#include "ParticleScreen.h"
#include "BaseMainScreen.h"
#include "CommonData.h"

//#import "SimpleAudioEngine.h"
//#import "CommonData.h"

//@class GameScreen;


USING_NS_CC;

Scene* TitleScreen::createScene()
{
    //シーンは一番大本の枠組み
    auto scene = Scene::create();
    //この上にボタン/Users/maruyamaosamu/Desktop/cat /cocos2dX/bbk/Classes/BaseMainScreen.cppやspliteを受け付けるレイヤー
    auto layer = TitleScreen::create();
    scene->addChild(layer);
    
	return scene;

}

bool TitleScreen::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    //サイズを取得
//    Size winSize = Director::getInstance()->getWinSize();
    //マルチスクリーンに対応した処理
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    
    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.
    
    // add a "close" icon to exit the progress. it's an autorelease object

#pragma mark - スプライトの作成

    
    Sprite* sprite = Sprite::create("nekotonezumi.png");//タイトル
    
//    Sprite* sprite = Sprite::create("Game/base/nekotonezumi.jpg");//タイトル
    
    //表示位置の決定
	sprite->setPosition(
                        Point(origin.x + visibleSize.width/2 ,
                              origin.y + visibleSize.height/2 ));
    
        
        //画像の大きさ指定　倍率
        sprite->setScale(0.5f);
        //画像をレイヤーに追加 z:高さ　大きい方が上に表示される
        this->addChild(sprite, 0);
    
        //メニューの作成
        Layer *layerB = Layer::create();
        this->addChild(layerB);
//titleScreenレイヤーに直接書いても良かったが
//一応、違うレイヤーを準備
    
//        CCLayer *layerB = [CCLayer node];
// レイヤーBをNewSceneレイヤーに追加します

//        [self addChild:layerB];
    
        // 前の画面に戻るメニューを作ります
    int bufY =60;
    //Ver3.x
    //autoは推論方宣言
    auto labelBtnLabel1 = LabelTTF::create("出陣！", "Arial", 48);
    labelBtnLabel1->setColor(Color3B(0, 255, 0));
    auto labelItem1 = MenuItemLabel::create(labelBtnLabel1,
    [&](Ref* pSender)->void{
        TransitionCrossFade* ts =TransitionCrossFade::create(1.0f,BaseMainScreen::createScene());
        Director::getInstance()->replaceScene(ts);
    });
    labelItem1->setPosition(Point(0,bufY*0));
    
    auto labelBtnLabel2 = LabelTTF::create("ステージセレクトへ", "Arial", 48);
    labelBtnLabel2->setColor(Color3B(255, 0, 0));
    auto labelItem2 = MenuItemLabel::create(labelBtnLabel2,
    [&](Ref* pSender)->void{
        TransitionCrossFade* ts =TransitionCrossFade::create(1.0f,BaseMainScreen::createScene());
        Director::getInstance()->replaceScene(ts);
    });
    labelItem2->setPosition(Point(0,bufY*1));

    /*
    auto labelBtnLabel3 = LabelTTF::create("テストへ", "Arial", 48);
    labelBtnLabel3->setColor(Color3B(0, 0, 255));
    auto labelItem3 = MenuItemLabel::create(labelBtnLabel3,
    [&](Ref* pSender)->void{
        TransitionCrossFade* ts =TransitionCrossFade::create(1.0f,BaseMainScreen::createScene());
        Director::getInstance()->replaceScene(ts);
    });
    labelItem3->setPosition(Point(0,bufY*2));
 */
    
    auto labelBtnLabel4 = LabelTTF::create("テスト画面へ", "Arial", 48);
    labelBtnLabel4->setColor(Color3B(0, 0, 0));
    auto labelItem4 = MenuItemLabel::create(labelBtnLabel4,
    [&](Ref* pSender)->void{
        
        TransitionFlipAngular* ts =TransitionFlipAngular::create(1.0f,ParticleScreen::createScene());
        Director::getInstance()->replaceScene(ts);
        
    });
    labelItem4->setPosition(Point(0,bufY*3));
    
    
    
    //後は同じ
    auto menu = Menu::create(labelItem1,labelItem2,labelItem4,NULL);
 
    menu->setPosition(visibleSize.width /2, visibleSize.height /10*2);
    this->addChild(menu);
    

        
        // CCManuItemImageを選択不可にします
//        labelBtnLabel2 = YES;
        




#pragma marks - BGM
    
//    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"gundam.mp3" loop:NO];
    
#pragma marks - ハイスコアの表示
//    [self setGameSaveSub];
    setGameSaveSub();
    CommonData* _commonData = CommonData::sharedCommonData();
 //   [self addChild:_commonData];//後で解放されるように
 
    std::string st ="ハイスコア ";
    std::stringstream ss;
    ss << _commonData->getHiscore();
    st.append(ss.str());
    
    auto text = Label::createWithSystemFont(st, "HiraKakuProN-W6", 48);
    text->setColor(Color3B(0,20,0));//色を指定する
    text->setPosition(
      Point(origin.x + visibleSize.width/2 ,
            origin.y + visibleSize.height/19*15 ));
    this->addChild(text);
    
    return true;
 }
 
    
/*

    NSString* startSt = [NSString stringWithFormat:@"%@ %d",@"ハイスコア ",_commonData.hiscore];
    CCLabelTTF* _dispHiScore = [CCLabelTTF labelWithString:startSt fontName:@"Arial-BoldMT" fontSize:20];
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    _dispHiScore.position = CGPointMake(winSize.width / 2, winSize.height /1-20);
       _dispHiScore.color = ccc3(0, 0, 0);
        [self addChild:_dispHiScore z:2];
        _dispHiScore.visible = YES;
        // 縦横のフォント幅が画像幅の0.8倍に収まるように調整します
        _dispHiScore.scale = 0.8;
//        [_dispHiScore setString:[NSString stringWithFormat:@"%d", _score]];

    
    return self;
}
*/
//-(void) setGameSaveSub
void TitleScreen::setGameSaveSub()
{
//    CommonData* _commonData = [CommonData sharedCommonData];
    CommonData* _commonData = CommonData::sharedCommonData();

    
    //ゲームスコア
    if(_commonData->getHiscore() < 0){//データをロード
        _commonData->setHiscore(0);
    }
    /*
    //報酬のセーブ
    _commonData.enemyNameNo =1;
    
    _commonData.enemyBonusKind1 =1;
    _commonData.enemyBonusNo1 =1;
    
    _commonData.enemyBonusKind2 =1 ;
    _commonData.enemyBonusNo2 =1 ;
    
    _commonData.enemyBonusKind3 =1 ;
    _commonData.enemyBonusNo3 =1 ;
*/
    _commonData->save();
///    [_commonData save];
    
}



/*


-(void)toggletouched{
CCLOG(@"CCMenuItemToggle selected.");
}

- (void)dealloc
{
    [super dealloc];
}


@end

*/