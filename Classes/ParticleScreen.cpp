//
//  ParticleScreen.m
//  blockRpg1
//
//  Created by weekfield on 2013/05/12.
//  Copyright 2013年 maro. All rights reserved.
//
// Import the interfaces
// Needed to obtain the Navigation Controller
//パーティクルの発生テスト

#include "ParticleScreen.h"
//#include "AppDelegate.h"
//#include "SimpleAudioEngine.h"

#include "TitleScreen.h"


USING_NS_CC;

Scene* ParticleScreen::createScene()
{
    //シーンは一番大本の枠組み
    auto scene = Scene::create();
    //この上にエフェクトをつける
    auto layer = ParticleScreen::create();
    scene->addChild(layer);
    
	return scene;
    
}


bool ParticleScreen::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
#pragma mark - スプライトの作成
    
    /*
    Sprite* sprite = Sprite::create("ikmen.jpg");//タイトル
    //表示位置の決定
	sprite->setPosition(
                          Point(origin.x + visibleSize.width/2 ,
                                origin.y + visibleSize.height/2 ));
    sprite->setScale(2.0f);
*/
    ParticleSystemQuad* particle = ParticleSystemQuad::create("Game/particle/stageEffect1.plist");

 //   particle->setAutoRemoveOnFinish(true);
    
    
    
    //表示位置の決定
	particle->setPosition(
                        Point(origin.x + visibleSize.width/2 ,
                              origin.y + visibleSize.height/2 ));
    
    
    //画像の大きさ指定　倍率
    //画像をレイヤーに追加 z:高さ　大きい方が上に表示される
    this->addChild(particle, 1);
//    this->addChild(sprite, 0);
    

    
    //戻るボタン----------------------------------------
    int bufY =60;
    //Ver3.x
    auto labelBtnLabel1 = LabelTTF::create("戻る！", "Arial", 48);
    labelBtnLabel1->setColor(Color3B(0, 255, 0));
    auto labelItem1 = MenuItemLabel::create(labelBtnLabel1, CC_CALLBACK_1(ParticleScreen::change, this));
    labelItem1->setPosition(Point(0,bufY*0));
    
    
    //後は同じ
    auto menu = Menu::create(labelItem1,NULL);
    
    menu->setPosition(visibleSize.width /2, visibleSize.height /10*2);
    this->addChild(menu);
    
    
    
    return true;
}


void ParticleScreen::change(Ref* pSender)
{
    
    TransitionCrossFade* ts =TransitionCrossFade::create(3.0f,TitleScreen::createScene());
    Director::getInstance()->replaceScene(ts);
}


/*
// on "init" you need to initialize your instance
-(id) init
{
	if( (self=[super init]) ) {
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		
        
        
        
        
        id particle = [CCParticleSystemQuad particleWithFile:@"yoko2.plist"];
        [self addChild:particle];
//        　パーティクルを表示する位置などは、他のCocos2dのオブジェクトと同じで
        [particle setPosition:ccp(200, 250)];
  //      　使い終わったらオートリリースする設定などは以下の形で設定。
        [particle setAutoRemoveOnFinish:YES];
         
   //     		[CCParticleSystemQuad particleWithFile:@"test.plist"];
        // 回転アニメーションオブジェクトを生成します
    //         id rotate = [CCRotateBy actionWithDuration:10 angle:360];
    //          id roop = [CCRepeatForever actionWithAction:rotate];
        // レイヤーAにアニメーションを適用します
    //            [self runAction:roop];
        
#pragma mark 敵を出す
        //敵を出す。
        
        //バッチノードを作り、そこにテキスチャーを読み込む
        CCSpriteBatchNode *spritesBgNode;
        spritesBgNode = [CCSpriteBatchNode batchNodeWithFile:@"blockRpgEnemy.png"];

        [self addChild:spritesBgNode];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"blockRpgEnemy.plist"];
        
        //配列にフレームを格納
        NSMutableArray *anim=[NSMutableArray arrayWithCapacity:1];
        for (int i=1; i<=2; i++) {
            [anim addObject:[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:[NSString stringWithFormat:@"monster%d.png",i]]];
        }
        
        //アニメーション設定
        CCAnimation *animation=[CCAnimation animationWithSpriteFrames:anim  delay:.2f];
        CCSprite*  _g1=[CCSprite spriteWithSpriteFrameName:@"monster1.png"];
        [_g1 runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:animation]]];
        [spritesBgNode addChild:_g1];
        
        _g1.position = ccp(winSize.width/2, winSize.height/2);


        
        
#pragma mark メニューの作成
        //メニューの作成
        CCLayer *layerB = [CCLayer node];
        [self addChild:layerB];
        // 前の画面に戻るメニューを作ります
        [CCMenuItemFont setFontName:@"Helvetica-BoldOblique"];
        [CCMenuItemFont setFontSize:25];
        
        CCMenuItemFont *item = [CCMenuItemFont itemWithString:@"タイトルへ" block:^(id sender) {
            CCTransitionFade *tran = [CCTransitionTurnOffTiles transitionWithDuration:1.0 scene:[TitleScreen scene] ];
            [[CCDirector sharedDirector] replaceScene:tran];
        }];
        item.color = ccc3(255,0,0);

        CCMenuItemFont *item2 = [CCMenuItemFont itemWithString:@"発動" block:^(id sender) {
            
            id particle = [CCParticleSystemQuad particleWithFile:@"yoko.plist"];
            [self addChild:particle];
            [particle setPosition:ccp(200, 250)];
            [particle setAutoRemoveOnFinish:YES];
            
        }];
        item.color = ccc3(255,255,0);

        
        CCMenuItemFont *item3 = [CCMenuItemFont itemWithString:@"ステージクリア" block:^(id sender) {
            CCTransitionFade *tran = [CCTransitionFade transitionWithDuration:1.0 scene:[BonusScreen scene] withColor:ccc3(255, 255, 255)];
            [[CCDirector sharedDirector] replaceScene:tran];
        }];
        item3.color = ccc3(255,255,255);
        
        
        CCMenu *menu = [CCMenu menuWithItems:item,item2,item3,  nil];
        // メニューが重なって表示されないように調整します
        [menu alignItemsVerticallyWithPadding:10.0];
        // メニューをレイヤーBに追加します
        [layerB addChild:menu];
	}
	return self;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}



#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}
@end

*/