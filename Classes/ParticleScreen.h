//
//  ParticleScreen.h
//  blockRpg1
//
//  Created by weekfield on 2013/05/12.
//  Copyright 2013年 maro. All rights reserved.
//
//
//  TitleScreen.h
//  blockRpg1
//
//  Created by 丸山 修 on 2013/04/29.
//  Copyright 2013年 maro. All rights reserved.
//
#ifndef __PARTICLE_SCREEN_H__
#define __PARTICLE_SCREEN_H__

#include "cocos2d.h"

class ParticleScreen : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    
    void change(Ref* pSender);
    CREATE_FUNC(ParticleScreen);//これないとcreateされない
};

#endif // __PARTICLE_SCREEN_H__

