//
//  CommonData.h
//  bbk
//
//  Created by weekfield2 on 2014/08/30.
//
//

#ifndef __bbk__CommonData__
#define __bbk__CommonData__

//#include <iostream>

#include "cocos2d.h" //基本的なインクルード

class CommonData: public cocos2d::Node
{//これ解放されるようにaddchildすべき＿
public:

    //モードの種類
    typedef enum{
        GameModeMenu ,
        GameModeNagasa ,
        GameModeKasa ,
    }GameMode;
    
    
    //モードの種類
    GameMode _gameMode;
    //    int _count;
    
    //ユーザー情報
    //    int _levelNo;
    
    //ゲーム開始時の情報
    //    int _areaNo;//エリア情報
    //    int _stageNo;//ステージ情報
    
  static CommonData* sharedCommonData();
//+(id)sharedCommonData;
    void save();
    void load();

    
/////////////////////////////////////////////////////////////////
//環境
/////////////////////////////////////////////////////////////////
private:
    
    void keyGetPutSub(bool getFg , cocos2d::ValueMap* persistentDic);//保存すべき情報をここで一括して保存
    void intKeySub(bool getFg, cocos2d::ValueMap*  persistentDic, int* idNo, std::string akey);

public:
    CC_SYNTHESIZE(int,hiscore,Hiscore);//ハイスコア情報
//@property (readwrite) int hiscore;//ハイスコア情報

    CC_SYNTHESIZE(int,nowAreaNo,NowAreaNo);//今のエリアＮＯ（この情報で敵や初期状態を決める）
    //@property (readwrite) int nowAreaNo;//今のエリアＮＯ（この情報で敵や初期状態を決める）
    
    CC_SYNTHESIZE(int,nowStageNo,NowStageNo)//今のステージＮＯ
    
    CC_SYNTHESIZE(int,clearStageBitNo,ClearStageBitNo)//クリアしたステージＮＯ マップでのステージの表示に必要　複数あるのでビット操作か？
    
    /////////////////////////////////////////////////////////////////
    //敵の持ち物
    /////////////////////////////////////////////////////////////////
    
    CC_SYNTHESIZE(int,enemyBonusKind1,EnemyBonusKind1)//ボーナス１ 敵がドロップするボーナスの種類１
    CC_SYNTHESIZE(int,enemyBonusKind2,EnemyBonusKind2)//ボーナス１ 敵がドロップするボーナスの種類２
    CC_SYNTHESIZE(int,enemyBonusKind3,EnemyBonusKind3)//ボーナス１ 敵がドロップするボーナスの種類３
    
    CC_SYNTHESIZE(int,enemyBonusNo1,EnemyBonusNo1)//ボーナス１ 敵がドロップするボーナス数１
    CC_SYNTHESIZE(int,enemyBonusNo2,EnemyBonusNo2)//ボーナス１ 敵がドロップするボーナス数２
    CC_SYNTHESIZE(int,enemyBonusNo3,EnemyBonusNo3)//ボーナス１ 敵がドロップするボーナス数３
    
    //倒した敵の種類
    CC_SYNTHESIZE(int,enemyNameNo,EnemyNameNo)//敵の名前ID
    
    
    /////////////////////////////////////////////////////////////////
    //自分の持ち物
    /////////////////////////////////////////////////////////////////
    
    CC_SYNTHESIZE(int,myFireElement,MyFireElement)//火のエレメント
    CC_SYNTHESIZE(int,myWaterElement,MyWaterElement)//水のエレメント
    CC_SYNTHESIZE(int,myWoodElement,MyWoodElement)//木のエレメント
    CC_SYNTHESIZE(int,myCoin,MyCoin)//コイン
    CC_SYNTHESIZE(int,myMainChara,MyMainChara)//自分のメインキャラ
    CC_SYNTHESIZE(int,myChara1,MyChara1)//持っているマイキャラ１
    CC_SYNTHESIZE(int,myChara2,MyChara2)//持っているマイキャラ２
    CC_SYNTHESIZE(int,myChara3,MyChara3)//持っているマイキャラ３
    

};




#endif /* defined(__bbk__CommonData__) */



