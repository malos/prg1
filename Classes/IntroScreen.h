//
//  IntroScreen.h
//  blockRpg1
//
//  Created by 丸山 修 on 2013/04/29.
//  Copyright maro 2013年. All rights reserved.
//



#ifndef __INTRO_SCREEN_H__
#define __INTRO_SCREEN_H__


#include "cocos2d.h"

class IntroScreen : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(IntroScreen);
};

#endif // __INTRO_SCREEN_H__
