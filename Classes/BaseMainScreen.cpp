//
//  BaseMainScreen.cpp
//  bbk
//
//  Created by weekfield2 on 2014/08/07.
//
//

#include "BaseMainScreen.h"

#include "BaseMainScreen.h"
#include "TitleScreen.h"
//#include "ShopScreen.h"
//#include "CommonData.h"
//#include "MyStatusLayer.h"
//#include "FighterStatusLayer.h"
//#include "MapMainScreen.h"

//#include "TouchSprite.h"

//@interface BaseMainScreen()


//@end


//@implementation BaseMainScreen


USING_NS_CC;

Scene* BaseMainScreen::createScene()
{
    //シーンは一番大本の枠組み
    auto scene = Scene::create();
    //この上にボタン/Users/maruyamaosamu/Desktop/cat /cocos2dX/bbk/Classes/BaseMainScreen.cppやspliteを受け付けるレイヤー
    auto layer = BaseMainScreen::create();
    scene->addChild(layer);
    
	return scene;
    
}



#pragma mark - 初期化
bool BaseMainScreen::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
#pragma mark ステータス画面の表示
        
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    Sprite* sprite = Sprite::create("Game/base/BaseMainSreen1.png");//基本画面のロード
    
    //表示位置の決定
	sprite->setPosition(
                        Point(origin.x + visibleSize.width/2 ,
                              origin.y + visibleSize.height/2 ));
    
    //画像の大きさ指定　倍率
    sprite->setScale(2.0f);
    //画像をレイヤーに追加 z:高さ　大きい方が上に表示される
    this->addChild(sprite, 0);
    
    
    //        [self addChild:layerB];
    // 前の画面に戻るメニューを作ります
    int bufY =60;
    //Ver3.x
    //文字アイテム１を作成-------------------
    auto labelBtnLabel1 = LabelTTF::create("出発！", "Arial", 48);
    labelBtnLabel1->setColor(Color3B(0, 255, 0));
    //ボタンアイテム１を作成
    auto labelItem1 = MenuItemLabel::create(labelBtnLabel1, CC_CALLBACK_1(BaseMainScreen::GoTitle, this));
    labelItem1->setPosition(Point(0,bufY*0));
    
    //文字アイテム2を作成-------------------
    auto labelBtnLabel2 = LabelTTF::create("会話", "Arial", 48);
    labelBtnLabel2->setColor(Color3B(255, 0, 0));
    //ボタンアイテム2を作成
    auto labelItem2 = MenuItemLabel::create(labelBtnLabel2, CC_CALLBACK_1(BaseMainScreen::GoTitle, this));
    labelItem2->setPosition(Point(0,bufY*1));
    
    auto labelBtnLabel3 = LabelTTF::create("合成屋", "Arial", 48);
    labelBtnLabel3->setColor(Color3B(0, 0, 255));
    auto labelItem3 = MenuItemLabel::create(labelBtnLabel3, CC_CALLBACK_1(BaseMainScreen::GoTitle, this));
    labelItem3->setPosition(Point(0,bufY*2));
    
    auto labelBtnLabel4 = LabelTTF::create("戻る", "Arial", 48);
    labelBtnLabel4->setColor(Color3B(0, 0, 0));
    auto labelItem4 = MenuItemLabel::create(labelBtnLabel4, CC_CALLBACK_1(BaseMainScreen::GoTitle, this));
    labelItem4->setPosition(Point(0,bufY*3));
    
    
    
    
    
    //後は同じ
    auto menu = Menu::create(labelItem1,labelItem2,labelItem3,labelItem4,NULL);
    
    menu->setPosition(visibleSize.width /2, visibleSize.height /10*2);
    this->addChild(menu,20);
    
/*
    
    
    
    
 
        //自分のステータスの表示
        [self addChild:[MyStatusLayer node] z:15 tag:TAG_BASE_MAIN_SCREEN_MY_STATUS];
        MyStatusLayer* _myStLayer = (MyStatusLayer*)([self getChildByTag:TAG_BASE_MAIN_SCREEN_MY_STATUS]);
        [_myStLayer statusXySet:winsize.width / 2 y:winsize.height / 10*3.5];
        
        
        //戦士の自分のステータスの表示
        [self addChild:[FighterStatusLayer node] z:15 tag:TAG_BASE_MAIN_SCREEN_FIGHTER_STATUS];
        FighterStatusLayer* _fighterStLayer = (FighterStatusLayer*)([self getChildByTag:TAG_BASE_MAIN_SCREEN_FIGHTER_STATUS]);
        [_fighterStLayer statusXySet:winsize.width / 2 y:winsize.height / 10*7.5];
        
        
        //バッチノードを作り、そこにテキスチャーを読み込む
        CCSpriteBatchNode *_spritesStageNode = [CCSpriteBatchNode batchNodeWithFile:@"mainParts.png"];
        
        [self addChild:_spritesStageNode z:15 tag:TAG_BASE_MAIN_SCREEN_BASE_PARTS_BATCH];
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"mainParts.plist"];
        
        float bx = winsize.width;
        float by = winsize.height;
        float const  dx = bx*0.02;
        //左矢印
        //      CCSprite *_arrow_l = [CCSprite
        TouchSprite *_arrow_l = [TouchSprite spriteWithSpriteFrameName:@"  arrow_left.png"];
        [_spritesStageNode addChild:_arrow_l z:15 tag:TAG_BASE_MAIN_SCREEN_BASE_PARTS_ARROW_L];
        _arrow_l.position = ccp(bx / 4,by / 1.8);
        //左矢印アクション
        id  moveTo1 = [CCMoveTo actionWithDuration:.2f position:ccp(bx/4-dx,by/1.8)];
        id  moveTo2 = [CCMoveTo actionWithDuration:.2f position:ccp(bx/4+dx,by/1.8)];
        id SequenceA  = [CCSequence actions:moveTo1,moveTo2,nil];
        id SequenceALOOP  = [CCRepeatForever actionWithAction:SequenceA];
        [_arrow_l runAction:SequenceALOOP];
        
        //右矢印
        TouchSprite *_arrow_r = [TouchSprite spriteWithSpriteFrameName:@"  arrow_right.png"];
        [_spritesStageNode addChild:_arrow_r z:15 tag:TAG_BASE_MAIN_SCREEN_BASE_PARTS_ARROW_R];
        _arrow_r.position = ccp(bx/ 1.3,by / 1.8);
        
        //右矢印アクション
        moveTo1 = [CCMoveTo actionWithDuration:.2f position:ccp(bx/1.3+dx,by/1.8)];
        moveTo2 = [CCMoveTo actionWithDuration:.2f position:ccp(bx/1.3-dx,by/1.8)];
        SequenceA  = [CCSequence actions:moveTo1,moveTo2,nil];
        SequenceALOOP  = [CCRepeatForever actionWithAction:SequenceA];
        [_arrow_r runAction:SequenceALOOP];
        
        
        
        
*/
        
    return true;
}


#pragma mark タイトルメニューの復帰作成
void BaseMainScreen::GoTitle(Ref* pSender)
{
    
    TransitionCrossFade* ts =TransitionCrossFade::create(1.0f,TitleScreen::createScene());
    Director::getInstance()->replaceScene(ts);
}


/*

#pragma mark - 通知関係
-(void) onEnter
{
	// スーパークラスのonEnterをコールします
	[super onEnter];
    
#pragma mark スケジュールを作る
    
    
    [self  scheduleUpdate];
    
}

#pragma mark - 割り込み処理
-(void) update:(ccTime)delta
{
    //   CommonData* _commonData = [CommonData sharedCommonData];
    //   MyStatusLayer* _myStLayer = (MyStatusLayer*)([self getChildByTag:TAG_BASE_MAIN_SCREEN_MY_STATUS]);
    
    CCSpriteBatchNode *_spritesStageNode = (CCSpriteBatchNode*)(
                                                                [self getChildByTag:TAG_BASE_MAIN_SCREEN_BASE_PARTS_BATCH]);
    
    TouchSprite *_arrow_l  = (TouchSprite*)([_spritesStageNode
                                             getChildByTag:TAG_BASE_MAIN_SCREEN_BASE_PARTS_ARROW_L]);
    TouchSprite *_arrow_r  = (TouchSprite*)([_spritesStageNode
                                             getChildByTag:TAG_BASE_MAIN_SCREEN_BASE_PARTS_ARROW_R]);
    
    FighterStatusLayer *_fighterStLayer  = (FighterStatusLayer*)
    ([self getChildByTag:TAG_BASE_MAIN_SCREEN_FIGHTER_STATUS]);
    
    CGSize winsize = [[CCDirector sharedDirector] winSize];
    
    
    
    switch (_mainStep){
            
        case STEP_BASE_MAIN_SCREEN_DISP_START:
            _mainStep= STEP_BASE_MAIN_SCREEN_DISP_WAIT;
            
            break;
        case STEP_BASE_MAIN_SCREEN_DISP_WAIT:
            
            if(_arrow_l.touchTrigger){
                _arrow_l.touchTrigger=false;
                _fightLayerOffsetX = 0;
                _mainStep= STEP_BASE_MAIN_SCREEN_ROTATE_L;
                
            }
            
            if(_arrow_r.touchTrigger){
                _arrow_r.touchTrigger=false;
                _fightLayerOffsetX = 0;
                _mainStep= STEP_BASE_MAIN_SCREEN_ROTATE_R;
                
            }
            
            
            //            [self setOkButton:layer];
            
            break;
        case STEP_BASE_MAIN_SCREEN_ROTATE_L:
            
            _fightLayerOffsetX -= 20;
            [_fighterStLayer statusXySet:winsize.width/2+
             _fightLayerOffsetX  y:winsize.height / 10*7.5];
            _mainStep= STEP_BASE_MAIN_SCREEN_ROTATE_L;
            
            if(_fightLayerOffsetX <-400){//完全に消えた
                _fightLayerOffsetX =400;
                _mainStep= STEP_BASE_MAIN_SCREEN_ROTATE_L2;
                
            }
            
            break;
            
        case STEP_BASE_MAIN_SCREEN_ROTATE_L2:
            
            _fightLayerOffsetX -= 20;
            [_fighterStLayer statusXySet:winsize.width/2+
             _fightLayerOffsetX  y:winsize.height / 10*7.5];
            
            
            if(_fightLayerOffsetX <=0){//元に戻った
                _fightLayerOffsetX =0;
                _mainStep= STEP_BASE_MAIN_SCREEN_DISP_WAIT;
                
            }
            break;
            
        case STEP_BASE_MAIN_SCREEN_ROTATE_R:
            
            _fightLayerOffsetX += 20;
            [_fighterStLayer statusXySet:winsize.width/2+
             _fightLayerOffsetX  y:winsize.height / 10*7.5];
            _mainStep= STEP_BASE_MAIN_SCREEN_ROTATE_R;
            
            if(_fightLayerOffsetX >400){//完全に消えた
                _fightLayerOffsetX =-400;
                _mainStep= STEP_BASE_MAIN_SCREEN_ROTATE_R2;
            }
            
            break;
            
        case STEP_BASE_MAIN_SCREEN_ROTATE_R2:
            
            _fightLayerOffsetX += 20;
            [_fighterStLayer statusXySet:winsize.width/2+
             _fightLayerOffsetX  y:winsize.height / 10*7.5];
            
            
            if(_fightLayerOffsetX >=0){//元に戻った
                _fightLayerOffsetX =0;
                _mainStep= STEP_BASE_MAIN_SCREEN_DISP_WAIT;
            }
            break;
            
            
            
            
        case STEP_BASE_MAIN_SCREEN_OK_WAIT://表示の待ち中
            
            break;
            
            
    }
    
    
}



@end
*/
